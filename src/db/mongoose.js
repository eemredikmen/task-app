const mongoose = require('mongoose')

const dbUrl = 'mongodb://127.0.0.1:27017/task-manager-api';

const options = {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true, 
}

mongoose.connect(dbUrl, options)
    .then( _ => console.log('Db Connect : OK'))
    .catch(err => console.log(err));

const express = require('express')
const Task = require('../models/task')
const { update } = require('../models/task')
const asyncHandler = require('express-async-handler');
const User = require('../models/user');
const router = new express.Router()

router.post('/tasks',asyncHandler(  async (req, res) => {
    const task = new Task(req.body)
        await task.save()
        res.status(201).send(task)
        res.status(400).send(e)
}))




router.get('/tasks', asyncHandler( async (req, res) => {
 
    const tasks = await Task.find({})
    res.send(tasks)
    res.status(500).send()
    
}))

router.get('/tasks/:id',asyncHandler(  async (req, res) => {
    const _id = req.params.id

    
        const task = await Task.findById(_id)

        if (!task) {
            return res.status(404).send()
        }

        res.send(task)
    
        res.status(500).send()
    
}))

router.patch('/tasks/:id',asyncHandler(  async (req, res) => {
    const updates = Object.keys(req.body)
    const allowedUpdates = ['description', 'completed']
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update))

    if (!isValidOperation) {
        return res.status(400).send({ error: 'Invalid updates!' })
    }  
        
        // const task = await Task.findByIdAndUpdate(req.params.id, req.body, { new: true, runValidators: true })

        const task = await Task.findById(req.params.id)

        updates.forEach((update) => task[update] = req.body[update])

        await task.save()


        if (!task) {
            return res.status(404).send()
        }

        res.send(task)
    
        res.status(400).send(e)
    
}))

router.delete('/tasks/:id', asyncHandler( async (req, res) => {
    
        const task = await Task.findByIdAndDelete(req.params.id)

        if (!task) {
            res.status(404).send()
        }

        res.send(task)
    
        res.status(500).send()
    
}))

module.exports = router
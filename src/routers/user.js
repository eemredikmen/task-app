const express = require('express')
const bycrpt = require('bcryptjs');
const User = require('../models/user')
const jtw = require('jsonwebtoken');
const asyncHandler = require('express-async-handler');
const router = new express.Router()

router.post('/users', asyncHandler(async (req, res) => {

    //* yeni user model

    const user = new User(req.body)
    //* user post gönderilmişse

    await user.save()

    res.status(201).send(user)

    //! gönderme kısmında hata varsa

    res.status(400).send('Eror')
}))

router.post('/users/login',async (req,res)=>{
    try{
        const user = await User.findByCredentials(req.body.email,req.body.password);

        const token = await user.generateAuthToken()
        
        res.send({user,token})
    }
    catch(e){
        console.log(e);
        res.status(400).send()
    }
})


router.get('/users', asyncHandler(async (req, res) => {

    //* model içerisndeki tüm alanları getir

    const users = await User.find({})

    res.send(users)

    res.status(500).send()

}))

router.get('/users/:id', asyncHandler(async (req, res) => {


    //* id değerini req.params eşitledik

    const _id = req.params.id


    //* model içerisndeki _id değerini aldıık

    const user = await User.findById(_id)

    //* id'li kullanıcı eşit değilse 

    if (!user) {
        res.status(404).send()
    }

    res.send(user)

    res.status(500).send()

}))

router.patch('/users/:id', asyncHandler(async (req, res) => {

    //* Object key ile req.bod içerisindeki key alanlarını aldık 

    const updates = Object.keys(req.body)

    //* güncellenilcek alanlar

    const allowedUpdates = ['name', 'email', 'password', 'age']

    //* alanlar güncellenilecek alanları isValidOperation değişkenine ekledik

    const isValidOperation = updates.every((update) => allowedUpdates.includes(update))

    //! güncellenmemiş ise

    if (!isValidOperation) {
        return res.status(400).send({ error: 'Invalid updates! 😠' })
    }

    const user = await User.findByIdAndUpdate(req.params.id, req.body, { new: true, runValidators: true })

    if (!user) {
        return res.status(404).send()
    }

    res.send(user)


    res.status(400).send(e)

}))

router.delete('/users/:id', asyncHandler(async (req, res) => {

    const user = await User.findByIdAndDelete(req.params.id)

    if (!user) {
        return res.status(404).send()
    }

    res.json({
        message : "Success",
        data : user
    })

    res.status(500).send()
}))

module.exports = router